//
//  Int+Extensions.swift
//  Test-Evir-Feature
//
//  Created by gcs test on 10/14/20.
//

extension Int {
    enum Converter {
        case tagType
        case extendedType
    }
    
    func getTypeValue(type: Converter) -> Int { // Int64 ?
        switch type {
        case .tagType:
            return (self & 0xF00000000000) >> 44
        case .extendedType:
            return (self & 0x03C000000000) >> 38
        }
        
    }
    
   
}
