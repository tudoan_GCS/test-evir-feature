//
//  EnumForTags.swift
//  Test-Evir-Feature
//
//  Created by gcs test on 10/14/20.
//

enum Source {
    case RFID, NFC
}

struct Tags {
    enum NFCType {
        case asset, extenedAsset, zone, driver, hubometer, student,
             auxiliary_3, auxiliar_4, auxiliar_5, auxiliar_6, auxiliar_7, auxiliar_8,
             unused_B, unused_D, unused_E, unused_F, unknown
        
        //        fun getDisplayableName() =  this.name.toLowerCase(Locale.getDefault())
        //                .replace(Regex("_[\\da-z]$"), "")
        //                .replace("_", " ")
        //                .trim()
    }
    
    enum ExtendedType {
        case standard, trailer, dolly, unknown
        
        //        fun getDisplayableName() =  this.name.toLowerCase(Locale.getDefault())
        //                .replace(Regex("_[\\da-z]$"), "")
        //                .replace("_", " ")
        //                .trim()
    }
}

enum ScanType {
    case asset, zone, badge, auxiliary
    
    //    fun getDisplayableName() =  this.name.toLowerCase(Locale.getDefault())
    //            .replace(Regex("_[\\da-z]$"), "")
    //            .replace("_", " ")
    //            .trim()
}

enum ScanResult {
    case parsedScan(source: Source,
                    rawData: Int, // Int64 ?
                    tagType: Tags.NFCType,
                    extendedType: Tags.ExtendedType?,
                    results: (ScanType, Int)) // Int64 ?
    
    case unknownScan(source: Source,
                      rawData :Int, // Int64 ?
                      tagType: Tags.NFCType?)
    
    case scanError(source: Source,
                    rawData: Int, // Int64 ?
                    errorMessage: String)
}
