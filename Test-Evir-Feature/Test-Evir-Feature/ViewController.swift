//
//  ViewController.swift
//  Test-Evir-Feature
//
//  Created by gcs test on 10/9/20.
//

import UIKit
import CoreNFC


class ViewController: UIViewController {
    @IBOutlet weak var inputLabel: UILabel!
    @IBOutlet weak var androidResultLabel: UILabel!
    @IBOutlet weak var iOSResultLabel: UILabel!
    @IBOutlet weak var inputTxt: UITextField!
    
    let strs = ["Input Data:", "Android-evir result:", "iOS-evir result:"]
    var nfcSession: NFCNDEFReaderSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inputTxt.isEnabled = false
    }
    
    @IBAction func startRun(_ sender: Any) {
        self.nfcSession = NFCNDEFReaderSession(delegate: self, queue: nil, invalidateAfterFirstRead: true)
        self.nfcSession?.alertMessage = "Hold near to scan"
        self.nfcSession?.begin()
    }
    
    func displayResult(result: String) {
        // decode ...
        guard let input = decodeData(str: result) else {
            print("Invalid tag length")
            
            // TODO: check NFC or RFID action
//            ScanResult.scanError(source: , rawData: 0, errorMessage: "Invalid tag length")
            return
        }
        
        let inputType = input.getTypeValue(type: .tagType)
        let numA = getNumberA(n: inputType)
        let numB = getNumberB(n: inputType)
        androidResultLabel.text = "\(strs[1]) \(numA)"
        iOSResultLabel.text = "\(strs[2]) \(numB)"
    }
    
    // decoding
    func decodeData(str: String) -> Int? {
        switch str.count {
        case 10:
            return Int((str + "00"), radix: 16)
        case 12:
            return Int(str, radix: 16)
        default:
            return nil
        }
    }
    
    func decodedScanResult() {
        
    }
    
    // android
    func getNumberA(n: Int) -> String {
        let data = (n & 0x0F)
        switch data {
        case 0x0: return "Zone"
        case 0x1: return "ASSET"
        default:
            return "empty"
        }
    }
    
    // ios
    func getNumberB(n: Int) -> String {
        switch n {
        case 0: return "Zone"
        case 1: return "ASSET"
        default:
            return "empty"
        }
    }
}


extension ViewController: NFCNDEFReaderSessionDelegate {
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        guard
            let readerError = error as? NFCReaderError,
            readerError.code != .readerSessionInvalidationErrorFirstNDEFTagRead,
            readerError.code != .readerSessionInvalidationErrorUserCanceled else {
            return
        }
        
        let alertController = UIAlertController(
            title: "Session Invalidated",
            message: error.localizedDescription,
            preferredStyle: .alert
        )
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        var result = ""
        for payload in messages[0].records {
            result += String.init(data: payload.payload.advanced(by: 3), encoding: .utf8)!
        }
        
        print("result = \(result)") // RAW value, expected value would be skip pre-fix by 4 or more characters
        DispatchQueue.main.async {
            self.inputTxt.text = result
            self.displayResult(result: result)
        }
    }
    
//    @available(iOS 13.0, *)
//    func readerSession(_ session: NFCNDEFReaderSession, didDetect tags: [NFCNDEFTag]) {
//        print("tags = \(tags)")
//    }
    
}
